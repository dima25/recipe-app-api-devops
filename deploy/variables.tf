variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "dmitriy.kozhevnikov@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgress instance"
}

variable "db_password" {
  description = "Password for the RDS postgress instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "748290218479.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-devops"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "748290218479.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-proxy"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}